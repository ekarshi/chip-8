package ins;

@FunctionalInterface
public interface OpCodeProcessor {
    void executeIns(short ins);
}
