package ins;

public interface InstructionHandler {
    void nextHandler(InstructionHandler handler);

    void handleIns(short ins);

    default void invalidState() {
        throw new RuntimeException("Invalid instruction encountered");
    }
}
