package ins;

import ins.opcodeimpl.*;

public enum OpCodeMap {
    _00EE(new _00EE()),
    _00E0(new _00E0()),
    _1NNN(new _1NNN()),
    _2NNN(new _2NNN()),
    _3XNN(new _3XNN()),
    _4XNN(new _4XNN()),
    _5XY0(new _5XY0()),
    _6XNN(new _6XNN()),
    _7XNN(new _7XNN()),
    _8XY0(new _8XY0()),
    _8XY1(new _8XY1()),
    _8XY2(new _8XY2()),
    _8XY3(new _8XY3()),
    _8XY4(new _8XY4()),
    _8XY5(new _8XY5()),
    _8XY6(new _8XY6()),
    _8XY7(new _8XY7()),
    _8XYE(new _8XYE()),
    _9XY0(new _9XY0()),
    _ANNN(new _ANNN()),
    _BNNN(new _BNNN()),
    _CXNN(new _CXNN()),
    _DXYN(new _DXYN()),
    _EX9E(new _EX9E()),
    _EXA1(new _EXA1()),
    _FX07(new _FX07()),
    _FX0A(new _FX0A()),
    _FX15(new _FX15()),
    _FX18(new _FX18()),
    _FX1E(new _FX1E()),
    _FX29(new _FX29()),
    _FX33(new _FX33()),
    _FX55(new _FX55()),
    _FX65(new _FX65()),
    _ONNN(null);

    private OpCodeProcessor opCodeProcessor;

    OpCodeMap(OpCodeProcessor opCodeProcessor) {
        this.opCodeProcessor = opCodeProcessor;
    }

    public OpCodeProcessor getOpCodeProcessor() {
        return opCodeProcessor;
    }
}
