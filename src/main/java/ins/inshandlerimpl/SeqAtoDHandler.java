package ins.inshandlerimpl;

import hw.Util;
import ins.InstructionHandler;
import ins.OpCodeMap;

public class SeqAtoDHandler implements InstructionHandler {
    private InstructionHandler instructionHandler;

    @Override
    public void nextHandler(InstructionHandler handler) {
        this.instructionHandler = handler;
    }

    @Override
    public void handleIns(short ins) {
        short highNibble = Util.extract(ins, (short) 4, (short) 13);
        if (highNibble == 0xA) OpCodeMap._ANNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 0xB) OpCodeMap._BNNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 0xC) OpCodeMap._CXNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 0xD) OpCodeMap._DXYN.getOpCodeProcessor().executeIns(ins);
        else if (null != instructionHandler) instructionHandler.handleIns(ins);
        else invalidState();
    }
}
