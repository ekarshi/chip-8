package ins.inshandlerimpl;

import hw.Util;
import ins.InstructionHandler;
import ins.OpCodeMap;

public class SeqEHandler implements InstructionHandler {
    private InstructionHandler instructionHandler;

    @Override
    public void nextHandler(InstructionHandler handler) {
        this.instructionHandler = handler;
    }

    @Override
    public void handleIns(short ins) {
        short highNibble = Util.extract(ins, (short) 4, (short) 13);
        if (highNibble == 0xE) {
            short lowByte = Util.extract(ins, (short) 8, (short) 1);
            if (lowByte == 0x9E) OpCodeMap._EX9E.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0xA1) OpCodeMap._EXA1.getOpCodeProcessor().executeIns(ins);
            else invalidState();
        } else if (null != instructionHandler) instructionHandler.handleIns(ins);
        else invalidState();
    }
}
