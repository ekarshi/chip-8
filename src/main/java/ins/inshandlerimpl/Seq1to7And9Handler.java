package ins.inshandlerimpl;

import hw.Util;
import ins.InstructionHandler;
import ins.OpCodeMap;

public class Seq1to7And9Handler implements InstructionHandler {
    private InstructionHandler instructionHandler;

    @Override
    public void nextHandler(InstructionHandler handler) {
        this.instructionHandler = handler;
    }

    @Override
    public void handleIns(short ins) {
        short highNibble = Util.extract(ins, (short) 4, (short) 13);
        if (highNibble == 1) OpCodeMap._1NNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 2) OpCodeMap._2NNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 3) OpCodeMap._3XNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 4) OpCodeMap._4XNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 5) OpCodeMap._5XY0.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 6) OpCodeMap._6XNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 7) OpCodeMap._7XNN.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 9) OpCodeMap._9XY0.getOpCodeProcessor().executeIns(ins);
        else if (null != instructionHandler) instructionHandler.handleIns(ins);
        else invalidState();
    }
}
