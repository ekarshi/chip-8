package ins.inshandlerimpl;

import hw.Util;
import ins.InstructionHandler;
import ins.OpCodeMap;

public class Seq0Handler implements InstructionHandler {
    private InstructionHandler instructionHandler;

    @Override
    public void nextHandler(InstructionHandler handler) {
        instructionHandler = handler;
    }

    @Override
    public void handleIns(short ins) {
        short highByte = Util.extract(ins, (short) 8, (short) 9);
        short lowByte = Util.extract(ins, (short) 8, (short) 1);
        short highNibble = Util.extract(ins, (short) 4, (short) 13);

        if (lowByte == 0xEE && highByte == 0x00) OpCodeMap._00EE.getOpCodeProcessor().executeIns(ins);
        else if (lowByte == 0xE0 && highByte == 0x00) OpCodeMap._00E0.getOpCodeProcessor().executeIns(ins);
        else if (highNibble == 0x0) passThrough(ins);
        else if (null != instructionHandler) instructionHandler.handleIns(ins);
        else invalidState();
    }

    private void passThrough(short ins) {
        System.out.println("Pass through " + ins);
    }
}
