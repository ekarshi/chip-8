package ins.inshandlerimpl;

import hw.Util;
import ins.InstructionHandler;
import ins.OpCodeMap;

public class SeqFHandler implements InstructionHandler {
    private InstructionHandler instructionHandler;

    @Override
    public void nextHandler(InstructionHandler handler) {
        this.instructionHandler = handler;
    }

    @Override
    public void handleIns(short ins) {
        short highNibble = Util.extract(ins, (short) 4, (short) 13);
        if (highNibble == 0xF) {

            short lowByte = Util.extract(ins, (short) 8, (short) 1);
            if (lowByte == 7) OpCodeMap._FX07.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0xA) OpCodeMap._FX0A.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x15) OpCodeMap._FX15.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x18) OpCodeMap._FX18.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x1E) OpCodeMap._FX1E.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x29) OpCodeMap._FX29.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x33) OpCodeMap._FX33.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x55) OpCodeMap._FX55.getOpCodeProcessor().executeIns(ins);
            else if (lowByte == 0x65) OpCodeMap._FX65.getOpCodeProcessor().executeIns(ins);
            else invalidState();

        } else if (null != instructionHandler) instructionHandler.handleIns(ins);
        else invalidState();
    }
}
