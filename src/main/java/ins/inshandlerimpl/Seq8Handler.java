package ins.inshandlerimpl;

import hw.Util;
import ins.InstructionHandler;
import ins.OpCodeMap;

public class Seq8Handler implements InstructionHandler {
    private InstructionHandler instructionHandler;

    @Override
    public void nextHandler(InstructionHandler handler) {
        this.instructionHandler = handler;
    }

    @Override
    public void handleIns(short ins) {
        short highNibble = Util.extract(ins, (short) 4, (short) 13);
        if (highNibble != 8) instructionHandler.handleIns(ins);
        else {
            short lowNibble = Util.extract(ins, (short) 4, (short) 1);
            if (lowNibble == 0) OpCodeMap._8XY0.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 1) OpCodeMap._8XY1.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 2) OpCodeMap._8XY2.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 3) OpCodeMap._8XY3.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 4) OpCodeMap._8XY4.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 5) OpCodeMap._8XY5.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 6) OpCodeMap._8XY6.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 7) OpCodeMap._8XY7.getOpCodeProcessor().executeIns(ins);
            else if (lowNibble == 0xE) OpCodeMap._8XYE.getOpCodeProcessor().executeIns(ins);
            else invalidState();
        }
    }
}
