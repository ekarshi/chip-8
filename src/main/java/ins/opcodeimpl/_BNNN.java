package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _BNNN implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short address = Util.extract(ins, (short) 12, (short) 1);
        Register.PC = (short) (Register.registers[0] + address);
    }
}
