package ins.opcodeimpl;

import hw.Memory;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _FX55 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        for (short k = 0; k <= X; k++)
            Memory.RAW[Register.I + k] = Register.registers[k];
    }
}
