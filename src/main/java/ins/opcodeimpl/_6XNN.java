package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _6XNN implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        byte NN = (byte) Util.extract(ins, (short) 8, (short) 1);
        short X = Util.extract(ins, (short) 4, (short) 9);
        Register.registers[X] = NN;
    }
}
