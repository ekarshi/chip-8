package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _ANNN implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        Register.I = Util.extract(ins, (short) 12, (short) 1);
    }
}
