package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

import java.util.Random;

public class _CXNN implements OpCodeProcessor {
    private static final Random random;

    static {
        random = new Random();
    }

    @Override
    public void executeIns(short ins) {
        short number = (short) random.nextInt(255);
        short nn = Util.extract(ins, (short) 8, (short) 1);
        short X = Util.extract(ins, (short) 4, (short) 9);
        Register.registers[X] = (byte) (number & nn);
    }
}
