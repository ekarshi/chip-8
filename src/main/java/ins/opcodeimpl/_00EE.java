package ins.opcodeimpl;

import hw.Memory;
import hw.Register;
import ins.OpCodeProcessor;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class _00EE implements OpCodeProcessor {
    private static ByteBuffer byteBuffer = ByteBuffer.allocate(2);

    static {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
    }

    @Override
    public void executeIns(short ins) {
        if (Register.SP - 1 < Memory.STACK_START)
            throw new RuntimeException("Stack return not possible");

        byteBuffer.put(Memory.RAW[Register.SP - 2]);
        byteBuffer.put(Memory.RAW[Register.SP - 1]);

        Register.PC = byteBuffer.getShort(0);

        Memory.RAW[Register.SP - 2] = 0;
        Memory.RAW[Register.SP - 1] = 0;

        Register.SP -= 2;
        byteBuffer.clear();
    }
}
