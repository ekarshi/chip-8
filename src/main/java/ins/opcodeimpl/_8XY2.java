package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _8XY2 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short Y = Util.extract(ins, (short) 4, (short) 5);
        Register.registers[X] = (byte) (Register.registers[X] & Register.registers[Y]);
    }
}
