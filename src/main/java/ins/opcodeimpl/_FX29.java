package ins.opcodeimpl;

import hw.KeyMap;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _FX29 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        Register.I = KeyMap.mapCode(Register.registers[X], false).getAddress();
    }
}
