package ins.opcodeimpl;

import hw.Memory;
import ins.OpCodeProcessor;

public class _00E0 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        for (short i = Memory.DISPLAY_START; i <= Memory.DISPLAY_END; i++) {
            Memory.RAW[i] = 0;
        }
    }
}
