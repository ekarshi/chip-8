package ins.opcodeimpl;

import hw.KeyBoard;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _EX9E implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        if (KeyBoard.checkKeyStatus(Register.registers[X])) Register.PC += 2;
    }
}
