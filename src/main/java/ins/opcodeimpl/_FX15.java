package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _FX15 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        Register.DT = Register.registers[X];
    }
}
