package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _9XY0 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short Y = Util.extract(ins, (short) 4, (short) 5);
        if (Register.registers[X] != Register.registers[Y]) Register.PC += 2;
    }
}
