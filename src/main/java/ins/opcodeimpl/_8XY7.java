package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _8XY7 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short Y = Util.extract(ins, (short) 4, (short) 5);
        int rx = Register.registers[X] & 0xFF;
        int ry = Register.registers[Y] & 0xFF;
        if (ry > rx) {
            Register.registers[X] = (byte) (ry - rx);
            Register.registers[0xF] = (byte) 1;
        } else {
            Register.registers[X] = (byte) (rx - ry);
            Register.registers[0xF] = (byte) 0;
        }
    }
}
