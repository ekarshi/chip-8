package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _8XY6 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        byte vxData = Register.registers[X];
        byte lsb = (byte) Util.extract(vxData, (short) 1, (short) 1);
        Register.registers[0xF] = lsb;
        Register.registers[X] = (byte) (Register.registers[X] >> 1);
    }
}
