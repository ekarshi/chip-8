package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _1NNN implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        Register.PC = Util.extract(ins, (short) 12, (short) 1);
    }
}
