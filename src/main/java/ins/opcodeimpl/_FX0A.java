package ins.opcodeimpl;

import hw.KeyBoard;
import hw.KeyMap;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _FX0A implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        System.out.println("waiting");
        while (true) {
            try{
                Thread.sleep(20, 0);
            }catch (InterruptedException ex){

            }
            if (!(KeyBoard.getLastKeyPressed() == KeyMap._UNKNOWN)) {
                Register.registers[X] = (byte) KeyBoard.getLastKeyPressed().getIndex();
                KeyBoard.resetLastKey();
                break;
            }
        }
    }
}
