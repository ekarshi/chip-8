package ins.opcodeimpl;

import hw.Memory;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _FX33 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short data = Register.registers[X];
        Memory.RAW[Register.I] = (byte) (data / 100);
        Memory.RAW[Register.I + 1] = (byte) ((data / 100) % 10);
        Memory.RAW[Register.I + 2] = (byte) (data % 10);
    }
}
