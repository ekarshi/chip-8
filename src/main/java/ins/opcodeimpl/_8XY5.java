package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _8XY5 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short Y = Util.extract(ins, (short) 4, (short) 5);
        int rx = Register.registers[X] & 0xFF;
        int ry = Register.registers[Y] & 0xFF;
        if (rx > ry) { //there will be no borrow
            Register.registers[0xF] = 1;
            Register.registers[X] = (byte) (rx - ry);
        } else { //there is borrow
            Register.registers[0xF] = 0;
            Register.registers[X] = (byte) (ry - rx);
        }
    }
}
