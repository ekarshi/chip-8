package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _3XNN implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short NN = Util.extract(ins, (short) 8, (short) 1);
        if (Register.registers[X] == (byte) NN) Register.PC += 2;
    }
}
