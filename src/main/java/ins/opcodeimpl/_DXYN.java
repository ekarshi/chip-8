package ins.opcodeimpl;

import hw.Memory;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _DXYN implements OpCodeProcessor {

    private static final byte[][] displayBits = new byte[32][64];
    private static final byte[] buffer = new byte[256];

    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short Y = Util.extract(ins, (short) 4, (short) 5);
        short N = Util.extract(ins, (short) 4, (short) 1);
        short row = Register.registers[Y];
        short col = Register.registers[X];
        refreshBitMap();
        for (int i = 0; i < N; i++) {
            String bString = Integer.toBinaryString((Memory.RAW[Register.I + i] & 0xFF) + 0x100).substring(1);
            xorToBitBuffer(row + i, col, bString);
        }

        bitCodeToByteBuffer();
        updateDisplayMemory();
    }

    private void xorToBitBuffer(int row, int col, String binary) {
        if (row > 31) row = 0;
        char[] data = binary.toCharArray();

        for (short i = 0; i < 8; i++) {
            if (col > 63) col = 0;
            byte given = displayBits[row][col];
            displayBits[row][col] = (byte) (displayBits[row][col] ^ (data[i] == '0' ? 0 : 1));
            if (given == 1 && displayBits[row][col] == 0) Register.registers[0xF] = (byte) 1;
            if (given == 0 && displayBits[row][col] == 1) Register.registers[0xF] = (byte) 0;
            col += 1;
        }
    }

    private void refreshBitMap() {
        short rows = (Memory.DISPLAY_END - Memory.DISPLAY_START + 1) / 8;
        for (short i = 0, curLoc = Memory.DISPLAY_START; i < rows; i += 1) {
            String rowAsBString = "";
            for (short j = 0; j < 8; j++) {
                rowAsBString += Integer.toBinaryString((Memory.RAW[curLoc] & 0xFF) + 0x100).substring(1);
                curLoc += 1;
            }
            updateBitMap(i, rowAsBString);
        }
    }

    private void updateBitMap(short row, String rowAsString) {
        short col = 0;
        for (char x : rowAsString.toCharArray()) {
            displayBits[row][col] = x == '1' ? (byte) 1 : (byte) 0;
            col += 1;
        }
    }

    private void bitCodeToByteBuffer() {
        short start = 0;
        for (short i = 0; i < displayBits.length; i++) {
            for (short j = 0; j < 64; j += 8) {
                String byteString = "";
                for (short k = j; k < j + 8; k++) {
                    byteString += Byte.toString(displayBits[i][k]);
                }
                buffer[start] = (byte) Short.parseShort(byteString, 2);
                start += 1;
            }
        }
    }

    private void updateDisplayMemory() {
        short start = Memory.DISPLAY_START;
        short loc = 0;
        while (start <= Memory.DISPLAY_END) {
            Memory.RAW[start] = buffer[loc];
            start += 1;
            loc += 1;
        }
    }
}
