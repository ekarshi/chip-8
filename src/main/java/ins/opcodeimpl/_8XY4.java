package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _8XY4 implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        short X = Util.extract(ins, (short) 4, (short) 9);
        short Y = Util.extract(ins, (short) 4, (short) 5);
        int rx = Register.registers[X] & 0xFF;
        int ry = Register.registers[Y] & 0xFF;
        Register.registers[0xF] = rx + ry > 255 ? (byte) 1 : (byte) 0;
        Register.registers[X] = (byte) (rx + ry);
    }
}
