package ins.opcodeimpl;

import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _8XYE implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        byte X = (byte) Util.extract(ins, (short) 4, (short) 9);
        byte data = Register.registers[X];
        Register.registers[0xF] = (byte) Util.extract(data, (short) 1, (short) 16);
        Register.registers[X] = (byte) (Register.registers[X] << 1);
    }
}
