package ins.opcodeimpl;

import hw.Memory;
import hw.Register;
import hw.Util;
import ins.OpCodeProcessor;

public class _2NNN implements OpCodeProcessor {
    @Override
    public void executeIns(short ins) {
        if (Register.SP > Memory.STACK_END) throw new RuntimeException("Stack call not possible");

        short jumpAddress = Util.extract(ins, (short) 12, (short) 1);
        short nextInsLoc = Register.PC;

        Memory.RAW[Register.SP] = (byte) (nextInsLoc >> 8);
        Memory.RAW[Register.SP + 1] = (byte) nextInsLoc;

        Register.SP += 2;
        Register.PC = jumpAddress;
    }
}
