import hw.Emulator;
import hw.ProgramLoader;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws IOException, URISyntaxException {
        ProgramLoader.loadInMemory("spaceinvaders.ch8");
        Emulator emulator = new Emulator();
        emulator.emulate();
    }
}
