package hw;

public class Register {

    public static byte[] registers = new byte[16]; // V0 - VF registers
    public static short PC; //Program counter
    public static short I; // 16 bit register similar to void pointer
    public static short SP; //Stack pointer
    public static byte DT; //Delay timer
    public static byte ST; //Sound timer
}
