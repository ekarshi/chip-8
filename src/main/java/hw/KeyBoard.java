package hw;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyBoard implements KeyListener {
    private static byte[] keyStates = new byte[16];
    private static KeyMap lastKeyPressed = KeyMap._UNKNOWN;

    public static boolean checkKeyStatus(short keyCode) {
        KeyMap keyMap = KeyMap.mapCode(keyCode, false);
        return keyStates[keyMap.getIndex()] == 1;
    }

    private static void switchState(KeyEvent event, byte state, boolean isReleased) {
        KeyMap keyMap = KeyMap.mapCode(event.getKeyCode(), true);
        if (keyMap != KeyMap._UNKNOWN) keyStates[keyMap.getIndex()] = state;
        if (!isReleased) lastKeyPressed = keyMap;
    }

    public static KeyMap getLastKeyPressed() {
        return lastKeyPressed;
    }

    public static void resetLastKey() {
        lastKeyPressed = KeyMap._UNKNOWN;
    }

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) {
        switchState(e, (byte) 1, false);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switchState(e, (byte) 0, true);
    }

}
