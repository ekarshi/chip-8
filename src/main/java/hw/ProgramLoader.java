package hw;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;

public class ProgramLoader {

    public static void loadInMemory(String name) throws IOException, URISyntaxException {
        //byte[] bytes = Files.readAllBytes(FileSystems.getDefault().getPath("/Users/ekarshi/chip8/", name));
        byte[] bytes = IOUtils.toByteArray(ProgramLoader.class.getClassLoader().getResource(name));
        int start = Memory.PROGRAM_START;
        for (int i = 0; i < bytes.length; i++, start++)
            Memory.RAW[start] = bytes[i];
        Register.PC = Memory.PROGRAM_START;
        Memory.PROGRAM_END = start;
        Register.SP = Memory.STACK_START;
    }
}
