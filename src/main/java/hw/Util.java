package hw;

public class Util {

    public static short extract(short number, short nBits, short position) {
        return (short) ((((1 << nBits) - 1) & (number >> (position - 1))));
    }
}
