package hw;

import java.util.EnumSet;

public class Memory {
    public static final short START = 0x0;
    public static final short END = 0xFFF;
    public static final short DISPLAY_START = 0xF00;
    public static final short DISPLAY_END = 0xFFF;
    public static final short STACK_START = 0xEA0;
    public static final short STACK_END = 0xEFF;
    public static final short PROGRAM_START = 0x200;
    public static final byte[] RAW = new byte[4096];
    public static int PROGRAM_END;
    private static short offset = START;

    //load 8x5 sprites into memory in the interpreter area
    static {
        EnumSet.allOf(KeyMap.class).forEach(key -> {
            int[] data = key.getSpriteData();
            if (null != data) {
                key.setAddress(offset);
                for (int i = 0; i < data.length; i++) {
                    RAW[offset + i] = (byte) data[i];
                }
                offset += 5;
            }
        });
    }
}
