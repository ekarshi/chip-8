package hw;

import javax.swing.*;
import java.awt.*;

public class Display {
    private final Video video;

    public Display() {
        video = new Video();
        JFrame jFrame = new JFrame("Chip-8 Video");
        jFrame.getContentPane().add(video);
        jFrame.setSize(new Dimension(794, 860));
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setResizable(false);
        video.requestFocusInWindow();
    }

    public void refresh() {
        video.paintImmediately(0, 0, 794, 860);
    }

    private static class Video extends JPanel {
        private static char[][] displayBuffer = new char[32][64];

        public Video() {
            this.setBackground(Color.BLACK);
            this.setForeground(Color.WHITE);
            this.addKeyListener(new KeyBoard());
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            g.setColor(Color.WHITE);
            g.setFont(new Font("Monospaced", Font.BOLD, 20));
            fillBuffer();
            int startY = 50;
            for (int i = 0; i < displayBuffer.length; i++) {
                g.drawString(bufferToString(i), 10, startY);
                startY += 20;
            }
        }

        private void fillBuffer() {
            short loc = Memory.DISPLAY_START;
            for (int i = 0; i < displayBuffer.length; i++) {
                int start = 0;
                for (int j = 0; j < 8; j++) {
                    String byteAsBinary = Integer.toBinaryString((Memory.RAW[loc] & 0xFF) + 0x100).substring(1);
                    updateBuffer(start, displayBuffer[i], byteAsBinary.toCharArray());
                    start += 8;
                    loc += 1;
                }
            }
        }

        private void updateBuffer(int start, char[] buffer, char[] given) {
            for (int i = start, j = 0; j < 8; i++, j++)
                buffer[i] = given[j] == '0' ? ' ' : 0x2588;
        }

        private String bufferToString(int col) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < displayBuffer[0].length; i++) {
                builder.append(displayBuffer[col][i]);
            }
            return builder.toString();
        }
    }
}
