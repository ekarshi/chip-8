package hw;

import ins.InstructionHandler;
import ins.inshandlerimpl.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Emulator {

    private InstructionHandler instructionHandler;
    private Display display;

    public Emulator() {
        Seq0Handler seq0Handler = new Seq0Handler();
        Seq1to7And9Handler seq1to7And9Handler = new Seq1to7And9Handler();
        Seq8Handler seq8Handler = new Seq8Handler();
        SeqAtoDHandler seqAtoDHandler = new SeqAtoDHandler();
        SeqEHandler seqEHandler = new SeqEHandler();
        SeqFHandler seqFHandler = new SeqFHandler();

        seq0Handler.nextHandler(seq1to7And9Handler);
        seq1to7And9Handler.nextHandler(seq8Handler);
        seq8Handler.nextHandler(seqAtoDHandler);
        seqAtoDHandler.nextHandler(seqEHandler);
        seqEHandler.nextHandler(seqFHandler);
        instructionHandler = seq0Handler;
        display = new Display();
        display.refresh();
    }

    public void emulate() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        long start = System.currentTimeMillis();
        while (true) {
            byteBuffer.put(Memory.RAW[Register.PC++]);
            byteBuffer.put(Memory.RAW[Register.PC++]);
            short ins = byteBuffer.getShort(0);
            System.out.println(String.format("%1$04X", ins) + " "
                    + Integer.toBinaryString(0xFFFF & ins)
                    + " PC at " + (Register.PC - 2));
            instructionHandler.handleIns(ins);
            byteBuffer.clear();
            if (Util.extract(ins, (short) 4, (short) 13) == 0xD) display.refresh();
            long after = System.currentTimeMillis();
            if ((after - start) > 50) {
                if (Register.DT > 0) Register.DT -= 1;
                start = System.currentTimeMillis();
            }
            if (Register.PC > Memory.PROGRAM_END) break;
        }
    }
}
