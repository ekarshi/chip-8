package hw;

public enum KeyMap {
    _0(48, 0, new int[]{0xF0, 0x90, 0x90, 0x90, 0XF0}),
    _1(49, 1, new int[]{0x20, 0x60, 0x20, 0x20, 0x70}),
    _2(50, 2, new int[]{0xF0, 0x10, 0xF0, 0x80, 0xF0}),
    _3(51, 3, new int[]{0xF0, 0x10, 0xF0, 0x10, 0xF0}),
    _4(52, 4, new int[]{0x90, 0x90, 0xF0, 0x10, 0x10}),
    _5(53, 5, new int[]{0xF0, 0x80, 0xF0, 0x10, 0xF0}),
    _6(54, 6, new int[]{0xF0, 0x80, 0xF0, 0x90, 0xF0}),
    _7(55, 7, new int[]{0xF0, 0x10, 0x20, 0x40, 0x40}),
    _8(56, 8, new int[]{0xF0, 0x90, 0xF0, 0x90, 0xF0}),
    _9(57, 9, new int[]{0xF0, 0x90, 0xF0, 0x10, 0xF0}),
    _A(65, 10, new int[]{0xF0, 0x90, 0xF0, 0x90, 0x90}),
    _B(66, 11, new int[]{0xE0, 0x90, 0xE0, 0x90, 0xE0}),
    _C(67, 12, new int[]{0xF0, 0x80, 0x80, 0x80, 0xF0}),
    _D(68, 13, new int[]{0xE0, 0x90, 0x90, 0x90, 0xE0}),
    _E(69, 14, new int[]{0xF0, 0x80, 0xF0, 0x80, 0xF0}),
    _F(70, 15, new int[]{0xF0, 0x80, 0xF0, 0x80, 0x80}),
    _UNKNOWN(0, 0, null);

    private int keyCode;
    private int index;
    private int[] spriteData;
    private short address;

    KeyMap(int keyCode, int index, int[] spriteData) {
        this.keyCode = keyCode;
        this.index = index;
        this.spriteData = spriteData;
    }

    public static KeyMap mapCode(int code, boolean emulatorToSystem) {
        KeyMap keyMap = KeyMap._UNKNOWN;

        if (code == (emulatorToSystem ? KeyMap._0.keyCode : KeyMap._0.index)) keyMap = KeyMap._0;
        if (code == (emulatorToSystem ? KeyMap._1.keyCode : KeyMap._1.index)) keyMap = KeyMap._1;
        if (code == (emulatorToSystem ? KeyMap._2.keyCode : KeyMap._2.index)) keyMap = KeyMap._2;
        if (code == (emulatorToSystem ? KeyMap._3.keyCode : KeyMap._3.index)) keyMap = KeyMap._3;
        if (code == (emulatorToSystem ? KeyMap._4.keyCode : KeyMap._4.index)) keyMap = KeyMap._4;
        if (code == (emulatorToSystem ? KeyMap._5.keyCode : KeyMap._5.index)) keyMap = KeyMap._5;
        if (code == (emulatorToSystem ? KeyMap._6.keyCode : KeyMap._6.index)) keyMap = KeyMap._6;
        if (code == (emulatorToSystem ? KeyMap._7.keyCode : KeyMap._7.index)) keyMap = KeyMap._7;
        if (code == (emulatorToSystem ? KeyMap._8.keyCode : KeyMap._8.index)) keyMap = KeyMap._8;
        if (code == (emulatorToSystem ? KeyMap._9.keyCode : KeyMap._9.index)) keyMap = KeyMap._9;
        if (code == (emulatorToSystem ? KeyMap._A.keyCode : KeyMap._A.index)) keyMap = KeyMap._A;
        if (code == (emulatorToSystem ? KeyMap._B.keyCode : KeyMap._B.index)) keyMap = KeyMap._B;
        if (code == (emulatorToSystem ? KeyMap._C.keyCode : KeyMap._C.index)) keyMap = KeyMap._C;
        if (code == (emulatorToSystem ? KeyMap._D.keyCode : KeyMap._D.index)) keyMap = KeyMap._D;
        if (code == (emulatorToSystem ? KeyMap._E.keyCode : KeyMap._E.index)) keyMap = KeyMap._E;
        if (code == (emulatorToSystem ? KeyMap._F.keyCode : KeyMap._F.index)) keyMap = KeyMap._F;

        return keyMap;
    }

    public int getIndex() {
        return index;
    }

    public int[] getSpriteData() {
        return spriteData;
    }

    public short getAddress() {
        return address;
    }

    public void setAddress(short address) {
        this.address = address;
    }
}
