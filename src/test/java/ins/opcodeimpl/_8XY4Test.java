package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.ByteBuffer;

@RunWith(JUnit4.class)
public class _8XY4Test {

    private _8XY4 opCode;
    private ByteBuffer byteBuffer;

    @Before
    public void setUp() {
        opCode = new _8XY4();
        byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) 129);
        byteBuffer.put((byte) 36);

    }

    @Test
    public void testAdditionNoCarry() {
        Register.registers[1] = 12;
        Register.registers[2] = 15;
        opCode.executeIns((byteBuffer.getShort(0)));

        Assert.assertEquals((byte) 27, Register.registers[1]);
        Assert.assertEquals(0, Register.registers[0xF]);
    }

    @Test
    public void testAdditionWithCarry() {
        Register.registers[1] = (byte) 250;
        Register.registers[2] = (byte) 250;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals((byte) 244, Register.registers[1]);
        Assert.assertEquals(1, Register.registers[0xF]);
    }
}