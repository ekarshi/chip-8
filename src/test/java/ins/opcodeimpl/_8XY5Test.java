package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.ByteBuffer;

@RunWith(JUnit4.class)
public class _8XY5Test {

    private _8XY5 opCode;
    private ByteBuffer byteBuffer;

    @Before
    public void setUpBeforeTest() {
        opCode = new _8XY5();
        byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) 129);
        byteBuffer.put((byte) 37);
    }

    @Test
    public void subtractWithoutBorrow() {
        Register.registers[1] = 5;
        Register.registers[2] = 2;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(3, Register.registers[1]);
        Assert.assertEquals(1, Register.registers[0xF]);
    }

    @Test
    public void subtractWithBorrow() {
        Register.registers[1] = (byte) 129;
        Register.registers[2] = (byte) 255;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals((byte) 126, Register.registers[1]);
        Assert.assertEquals(0, Register.registers[0xF]);
    }

}