package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.ByteBuffer;


@RunWith(JUnit4.class)
public class _5XY0Test {

    private _5XY0 opCode;
    private ByteBuffer byteBuffer;

    @Before
    public void setUp() {
        opCode = new _5XY0();
        byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) 81);
        byteBuffer.put((byte) 32);
    }

    @Test
    public void shouldSkipNextInstructionIfEqual() {
        Register.registers[1] = 1;
        Register.registers[2] = 1;
        Register.PC = 514;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(516, Register.PC);
    }

    @Test
    public void shouldNotSkipInstructionIfNotEqual() {
        Register.registers[1] = 1;
        Register.registers[2] = 2;
        Register.PC = 514;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(514, Register.PC);
    }
}