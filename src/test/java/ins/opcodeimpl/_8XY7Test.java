package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.ByteBuffer;

@RunWith(JUnit4.class)
public class _8XY7Test {

    private _8XY7 opCode;
    private ByteBuffer byteBuffer;

    @Before
    public void setUp() {
        opCode = new _8XY7();
        byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) 129);
        byteBuffer.put((byte) 39);
    }

    @Test
    public void subtractWithoutBorrow() {
        Register.registers[2] = 7;
        Register.registers[1] = 1;
        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(6, Register.registers[1]);
        Assert.assertEquals(1, Register.registers[0xF]);
    }

    @Test
    public void subtractWithBorrow() {
        Register.registers[2] = 1;
        Register.registers[1] = (byte) 255;
        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals((byte) 254, Register.registers[1]);
        Assert.assertEquals(0, Register.registers[0xF]);
    }

}