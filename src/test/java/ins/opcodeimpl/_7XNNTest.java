package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class _7XNNTest {
    private _7XNN opCode;

    @Before
    public void setUp() {
        opCode = new _7XNN();
        Register.registers[4] = 10;
    }

    @Test
    public void testAddition() {
        short givenIns = 0b0111010001111000;

        opCode.executeIns(givenIns);

        Assert.assertEquals((byte) 130, Register.registers[4]);
    }
}