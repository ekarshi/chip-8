package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.ByteBuffer;

@RunWith(JUnit4.class)
public class _4XNNTest {

    private _4XNN opCode;
    private ByteBuffer byteBuffer;

    @Before
    public void setUp() {
        opCode = new _4XNN();
        byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) 49);
        byteBuffer.put((byte) 25);
    }

    @Test
    public void shouldSkipNextInstructionIfNotEqual() {
        Register.registers[1] = 24;
        Register.PC = 514;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(516, Register.PC);
    }

    @Test
    public void shouldNotSkipNextInstructionIfEqual() {
        Register.registers[1] = 25;
        Register.PC = 514;

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(514, Register.PC);
    }

}