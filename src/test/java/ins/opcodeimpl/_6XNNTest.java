package ins.opcodeimpl;

import hw.Register;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.ByteBuffer;

@RunWith(JUnit4.class)
public class _6XNNTest {

    private _6XNN opCode;

    @Test
    public void testAssignment() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.put((byte) 97);
        byteBuffer.put((byte) 25);
        Register.registers[1] = 0;
        opCode = new _6XNN();

        opCode.executeIns(byteBuffer.getShort(0));

        Assert.assertEquals(25, Register.registers[1]);

    }
}